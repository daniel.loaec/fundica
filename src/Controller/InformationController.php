<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\Contact\Domain1ContactType;
use App\Form\Contact\Domain2ContactType;
use App\Form\Contact\Domain3ContactType;
use App\Form\FundicaContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class InformationController extends AbstractController
{
    // Should be extracted in a more common place
    const DOMAIN_1 = 'domain1';
    const DOMAIN_2 = 'domain2';
    const DOMAIN_3 = 'domain3';

    private function determineContactForm(): string
    {
        // determine current domain, using a service or some re-usable code
        $domain = "xxx";

        switch ($domain) {
            case self::DOMAIN_1:
                return Domain1ContactType::class;
            case self::DOMAIN_2:
                return Domain2ContactType::class;
            case self::DOMAIN_3:
                return Domain3ContactType::class;
            default:
                throw $this->createNotFoundException('No contact form for current domain');
        }
    }

    public function contact(): Response
    {
        $contact = new Contact();
        $form = $this->createForm($this->determineContactForm(), $contact);

        // generate form and send it to template engine, or,
        // handle and validate submitted data
    }
}
