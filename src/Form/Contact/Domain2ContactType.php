<?php

namespace App\Form\Contact;

use App\Form\FundicaContactType;
use Symfony\Component\Form\FormBuilderInterface;

class Domain2ContactType extends FundicaContactType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->remove('phoneNumber');
    }
}
