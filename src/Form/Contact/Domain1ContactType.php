<?php

namespace App\Form\Contact;

use App\Form\FundicaContactType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class Domain1ContactType extends FundicaContactType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('firstName', TextType::class);
    }
}
