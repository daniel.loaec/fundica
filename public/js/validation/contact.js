// <form id="contact" name="contact"...>
//    <input id="phoneNumber" name ="phoneNumber"...>
//    <input id="email" name ="email"...>
//    ...
// </form>

const form = document.getElementById('form');
const phoneNumber = document.getElementById('phoneNumber');
const email = document.getElementById('email');

form.onsubmit = (e) => {
    if (phoneNumber && !phoneNumber.value.match(/phone_number_constraint_regex/)) {
        e.preventDefault()
        triggerError('invalid phone number')
    }
    if (email && !email.value.match(/email_constraint_regex/)) {
        e.preventDefault()
        triggerError('invalid email address')
    }
}

const triggerError = (message) => {
    // alert, toast, whatever... the user
}
